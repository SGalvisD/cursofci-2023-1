# Donde estan los plos de los  microestados?
# diferencia entre los dos casos???
#4.0
import numpy as np
import matplotlib.pyplot as plt

class Dados():

    def __init__(self,N_Dados,Nmax = 1e6):
        self.N = int(N_Dados)
        self.Nmax = int(Nmax)

    
    def Conteo_s(self):
        Conteo = np.zeros(self.Nmax)

        for n_ in range(self.Nmax):
            Evento = np.random.randint(1,7,size =self.N)
            Conteo[n_] = Evento.sum()

        return Conteo
    
    def Conteo_1a1(self):
        Conteo = np.zeros(self.Nmax)

        for n_ in range(self.N):
            Evento = np.random.randint(1,7,size = self.Nmax)
            Conteo += Evento

        return Conteo
    
    def Graf(self,Tipo = '1a1'):
        if Tipo == '1a1':
            Valores = self.Conteo_1a1()
        elif Tipo == 's':
            Valores = self.Conteo_s()
        else:
            print('Caso no válido:\nTipo debe ser = \n"1a1" para Conteo 1 a 1  o \n"s" para Conteo Simultaneo')
        
        plt.figure()


        plt.hist(Valores,bins=(6*self.N-self.N),edgecolor='black',linewidth=1.2)

        plt.xticks(np.arange(self.N,6*self.N+1,1))
        plt.xlabel('Suma')
        plt.ylabel('N Eventos')
        plt.grid()
         
        plt.savefig('Plot_Eventos_Dados.png')