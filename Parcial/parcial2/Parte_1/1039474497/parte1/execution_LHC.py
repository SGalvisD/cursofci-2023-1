# Diferencia entre los dos colisionadores
#4.5
from LHC import ColisionadorCircular

if __name__=="__main__":
    print("ejecutando LHC")

        # Instanciar el colisionador y ejecutar la simulación
    R = 5  # Radio del colisionador circular
    r = 1  # Radio de las partículas
    n_values = [10, 100, 1000, 10000]  # Cantidad de partículas a simular
    print("En esta versión adaptada, se generan los ángulos aleatorios theta1 y theta2 en el rango de 0 a 2π, que representan la posición angular de las partículas en las caras A y B del colisionador circular, respectivamente. Luego, se calculan las coordenadas x1, y1, x2 y y2 correspondientes a las partículas en ambas caras.")

    colisionador = ColisionadorCircular(R, r, n_values)
    colisionador.graficar_probabilidad_de_colision()
    