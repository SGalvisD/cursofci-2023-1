# La conclusion respecto al colicionador circular?
# 3.0
import numpy as np
import matplotlib.pyplot as plt
import math 


R=100/math.sqrt(math.pi) #Radio del colisionador debe ser un numero mayor a cero
r=10 #Radio de la particula
n=np.arange(10,100000,1000) #Numero de iteraciones

pr=[]
for i in n:
    x1,y1=np.random.uniform(-R,R,[2,i]) #cara A
    x2,y2=np.random.uniform(-R,R,[2,i]) #Cara B
    exp_v=((x1**2+y1**2)<(R-r)**2) & ((x2**2+y2**2)<(R-r)**2)
    colisiones=np.vectorize(lambda x1,y1,x2,y2:True if math.dist([x1,y1],[x2,y2]) <=2*r else False )\
        (x1[exp_v],y1[exp_v],x2[exp_v],y2[exp_v])
    pr.append(colisiones.sum()/exp_v.sum())

plt.plot(n, pr)
plt.title("Convergencia probabilidad colisión")
plt.xlabel("N")
plt.ylabel("Probabilidad")
plt.savefig("LHC.png")