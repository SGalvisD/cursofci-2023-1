import numpy as np

# Definamos la clase para colisión
class colision:
    
    def __init__(self,R,r,N): 
        self.R=R # Radio del colisionador
        self.r=r # Radio de las partículas
        self.N=N # Número de experimentos
    
    # Método para generar posiciones aleatorias dentro de la zona de interés

    def posicion( self ):
        
        self.x1 = []
        self.y1 = []
        self.x2 = []
        self.y2 = []

        for i in range (0,self.N):
            self.x1.append(np.random.uniform(-(self.R - self.r), self.R - self.r))
            self.y1.append(np.random.uniform(-(self.R - self.r), self.R - self.r))
            self.x2.append(np.random.uniform(-(self.R - self.r), self.R - self.r))
            self.y2.append(np.random.uniform(-(self.R - self.r), self.R - self.r))

        return self.x1,self.y1,self.x2,self.y2

    # Método para calcular la probabilidad de choque

    def impacto( self ):

        num_choques  = []
        probabilidad = []
        
        for m in range( 0, self.N ):

            distancia = np.sqrt( (self.x1[m]-self.x2[m])**2 + (self.y1[m]-self.y2[m])**2 )
             
            if ( distancia <= 2*self.r ):
                num_choques.append(1)
            else:
                num_choques.append(0)

            prob = sum( num_choques )/ len( num_choques )
            probabilidad.append(prob)

        return probabilidad
