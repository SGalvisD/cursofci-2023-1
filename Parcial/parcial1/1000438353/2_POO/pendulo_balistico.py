import numpy as np
import matplotlib.pyplot as plt
import sys

class PenduloBalistico():
    
    def __init__(self,m,M,R,theta,g,t):
        #print("Iniciando clase PenduloBalistico\n")

        self.m=m
        self.M=M
        self.theta=np.deg2rad(theta)
        self.g=g
        self.R=R
        self.t=np.linspace(0,t,10000)
    
    def VelBala(self):
       if self.theta<np.deg2rad(180):
          velbala=2*(self.M+self.m)*np.sin(self.theta/2)/self.m * np.sqrt(self.g*self.R)
          return velbala
       else:
          print("Ingrese un ángulo menor a 180")
          sys.exit()

    def Graph(self):
       font = {'weight' : 'bold', 'size'   : 18}
       plt.rc('font', **font)
       plt.figure(figsize=(10,8))

       # las oscilaciones del péndulo están descritas por una función coseno
       # debido a que para t=0 se toma que la velocidad angular es 0
       # la constante de integración es el angulo inicial
       plt.title("Oscilación del péndulo balistico")
       plt.plot(self.t,np.rad2deg(self.theta*np.cos(np.sqrt(self.g/self.R)*self.t)),lw=2)
       plt.xlabel("Tiempo [s]")
       plt.ylabel("Ángulo [°]")
       plt.grid()
       plt.savefig("Oscilacion_pendulo.png")
       print("Se ha guardado las oscilaciones del péndulo balistico como Oscilacion_pendulo.png")
    

class PenduloBalistico2(PenduloBalistico):
    def __init__(self,m,M,R,g,t,u0,theta):
        super().__init__(m,M,R,theta,g,t)
        self.u0=u0
    
    # velocidad de la bala para que el sistema bala bloque de una vuelta
    def VelMinBala(self):
        velminbala=(self.m+self.M)/self.m*np.sqrt(5*self.g*self.R)
        return velminbala

    def VelSis(self):
        velsis=self.m/(self.m+self.M)*self.VelBala()
        return velsis