from pendulosimple import pendulosimple

if __name__=="__main__":
    a=0
    b=5
    wn=4
    theta0=1
    w0=0

    f1=lambda w: w
    f2=lambda theta: -wn**2*theta  

    sol=pendulosimple(a,b,wn,theta0,w0,f1,f2)
    print("Metodo de euler: {}".format(sol.euler()[0]))
    print("Metodo analitico: {}".format(sol.metodo_analitico()[1]))
    graf=sol.desplazamientoAngular()