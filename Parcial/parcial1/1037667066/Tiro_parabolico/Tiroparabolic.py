import numpy as np
import matplotlib.pyplot as plt
import math 

#creación de la clase
class tiro():
    def __init__(self,v0,angulo,y0,x0,gravedad,aire,tiempo,grafica):

        #Constructor
        self.v0=v0 #velocidad inicial del objeto [m/s]
        self.radianangulo=math.radians(angulo) #angulo de lanzamiento [grados]
        self.y0=y0  #altura inicial en y [m]
        self.x0=x0  #posicion en x inicial [m]
        self.gravedad=gravedad #gravedad (Aceleración en y) [m/s^2]
        self.aire=aire #Aceleración proporcionada por un viento en contra del movimiento (Aceleracion en x) [m/s^2]
        self.t=tiempo #tiempo en el cual se quiere determinar la velocidad [s]
        self.grafica=grafica #grafica 1 y 2
    
    #metodos para el Tiro Parabolico
    def v0x(self): #velocidad inicial en x
        return self.v0*np.cos(self.radianangulo)
    
    def v0y(self): #velocidad inicial en y
        return self.v0*np.sin(self.radianangulo)
    
    def velocidadenx(self): #velocidad en x para cualquier instante de tiempo
        if self.t > self.tiempodevuelo():
            return 0.0
        else:
            return self.v0x()-(self.aire*self.t)
    
    def velocidadeny(self): #velocidad en y para cualquier instante de tiempo
        if self.t > self.tiempodevuelo():
            return 0.0 
        else:
            return self.v0y()-(self.gravedad*self.t)
    
    def tiempodevuelo(self):#tiempo de vuelo
        if self.gravedad == 0:
            raise ValueError('Error cambie gravedad')
        else:
            tiempodevuelo=(self.v0y()+np.sqrt(self.v0y()**2+2*self.gravedad*self.y0))/(self.gravedad)
            return tiempodevuelo
    
    def tm(self): #tiempo para el cual v0y=0 (tiempo cuando la altura es maxima)
        if self.gravedad == 0:
            raise ValueError('Error cambie gravedad')
        else:
            return self.v0y()/self.gravedad
    
    def alturamax(self): #Altura maxima
        return self.y0+self.v0y()*self.tm()-(1/2)*self.gravedad*self.tm()**2

    def alcancemax(self): #Alcance maximo
        return self.x0+self.v0x()*self.tiempodevuelo()-(1/2)*self.aire*self.tiempodevuelo()**2
    
    def x(self): #posición en x
        t=np.linspace(0,self.tiempodevuelo(),100)
        return self.x0+self.v0x()*t-(1/2)*self.aire*t**2
    
    def y(self): #Posicion en y
        t=np.linspace(0,self.tiempodevuelo(),100)
        return self.y0+self.v0y()*t-1/2*self.gravedad*t**2
    
    def graf(self):#grafica
        plt.figure(figsize=(10,9))
        plt.plot(self.x(), self.y())
        plt.plot(self.y0,'o',color='green')
        plt.title(self.grafica,fontsize=15)
        plt.xlabel('x (m)',fontsize=15)
        plt.ylabel('y (m)',fontsize=15)
        plt.axhline(y=0,color='red')
        plt.grid()
        plt.savefig(self.grafica)

class tiro2(tiro): #subclase tiro2
      '''
      Consideremos que se lanza un objeto a una altura inicial y0, en este herencia vamos a permitir que el objeto caiga 
      por debajo de la altura y=0, por ejemplo, considerar el caso donde el objeto se lanza desde un acantilado. Para ello
      debemos considerar un nuevo atributo que es la profundidad del acantilado, esto trae consigo una modificación en los metodos
      del tiempo de vuelo y de la altura maxima. El programa nos retorna el tiempo de vuelo, el alcance maximo y la altura maxima. 
      ''' 

      def __init__(self,v0,angulo,y0,x0,gravedad,aire,tiempo,grafica,hp):
        super().__init__(v0,angulo,y0,x0,gravedad,aire,tiempo,grafica)
        self.profundidad=hp #Profundidad del acantilado [m]

      def tiempodevuelo(self): #Modificación del metodo del tiempo de vuelo
            if self.gravedad == 0:
                raise ValueError('Error cambie gravedad')
            else:
                tiempodevuelo=(self.v0y()+np.sqrt(self.v0y()**2+2*self.gravedad*(self.y0+self.profundidad)))/(self.gravedad)
                return tiempodevuelo 
            
      def alturamax(self): #Modificación del metodo altura maxima
          return super().alturamax() + self.profundidad 
      
      
      

        
