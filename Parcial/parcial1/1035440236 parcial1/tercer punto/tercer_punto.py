import numpy as np
import  matplotlib.pyplot as plt
class diferenciales ():
    def __init__(self,h,f,xo,yo):
        self.h=h
        self.xo=xo
        self.yo=yo
        self.f=f
        
        
        

    def recurrencia(self):
        contadorx=[]*12
        contadory=[]*12
        suma=0
        m=[0]*12
        m[0]=self.yo
        m.append(self.yo)
        y_1=self.yo+self.h*self.f(self.xo)
        for i in range (0,11):
        
            x_n=self.xo+self.h*i  # x se va actualizando
            contadorx.append(x_n) # guardo los valores de x actualizados
             # valor de i
            yn = m[i]

            yit =yn+self.h*self.f(x_n)
            m[i+1]=yit
            contadory.append(yn)
        print(contadorx)    
        return contadory
    def arreglo(self):
        x=np.array([0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1])
        return x
    def funcion_analica(self):
        funcion=-np.exp(-self.arreglo())
        return funcion
    def grafica(self):
        plt.figure(figsize=(10,8))
        plt.plot(self.arreglo(), self.funcion_analica(),color="purple",lw=3,label="analitica")
        plt.plot(self.arreglo(),self.recurrencia(),label="numerica")
        
        
        plt.xlabel(" x")
        plt.ylabel(" y")
        plt.grid()
        plt.title("solucion analitica y numerica")
        plt.savefig("diferenciales_grafica.png")
        
        plt.show()
    def error(self):
        plt.figure(figsize=(10,8))
        plt.plot(self.arreglo(), self.funcion_analica()-self.recurrencia(),color="green",lw=3,label="analitica")
        plt.xlabel(" x")
        plt.ylabel(" y")
        plt.grid()
        plt.title("error")
        plt.savefig("error_grafica.png")
        plt.show()




    
        
    
    
            
        
        
        #print(len(contadorx))
        #plt.figure(figsize=(10,8))
        #plt.plot(contadorx, contadory)
        #plt.savefig("diferenciales.png")
        #plt.show()