from EDO import solnumerica
from EDO import PenduloSimple
import sympy as sp

if __name__=="__main__":

    #-------------------Ejercicio 1------------------- #defino parametros para la solucion numerica y analitica de e^-x
    a,b,n,y0 = 0,1,100,-1
    def f(x):
        return sp.exp(-x) #defino la funcion a integrar con sp pues con np no funciona el codigo de sympy
    sol_num = solnumerica(a,b,n,y0,f)
    sol_num.plot() #muestro la solucion numerica y analitica

    #-------------------Ejercicio 2------------------- #defino parametros para el pendulo simple
    w0 = 4 #frecuencia angular
    a = 0 #limite inferior del intervalo de integracion
    b = 5 #limite superior del intervalo de integracion
    h = 0.1 #paso de integracion  )
    y0 = [1,0] #condiciones iniciales theta0,omega0
    def f(x):
        return -w0**2*x #defino la funcion a integrar

    psol_numerica = PenduloSimple(a,b,0,y0,f,w0,h) #creo un objeto de la clase PenduloSimple
    psol_numerica.plot() #muestro resultados
    