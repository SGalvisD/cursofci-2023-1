import numpy as np

#El siguiente código calcula la velocidad de la bala en función del ángulo de desviación del bloque que se le ingrese
# los parámteros de entrada son: angulo de desviación, masa de la bala, masa del bloque, radio del bloque, gravedad
#la segunda clase calcula la velocidad mínima para que el bloque de una vuelta completa y grafica la trayectoria del sistema masa-bloque-bala
#los parámetros de entrada son: angulo de desviación, masa de la bala, masa del bloque, radio del bloque, gravedad, tiempo de oscilación del bloque

class Pendulus(): #calculo la velocidad de la bala  dado un ángulo de desviación
    def __init__(self,ang,m,M,R,g):
        self.ang = ang*np.pi/180 #angulo de desviacion del bloque
        self.m = m #mass of the bullet
        self.M = M #mass of the pendulus
        self.g = g #gravity
        self.R = R #radius of the pendulus
    
    def DesviacionAngulo(self):
        return np.sqrt(2*self.g*self.R*(1-np.cos(self.ang))) #dado el angulo de desviacion me devuelve velociti masa-bloque
    
    def VelocidadBala(self):
        return (self.m+self.M)*self.DesviacionAngulo()/self.m #velocidad de la bala sacada de la velocidad masa-bloque
    
class Pendulus2(Pendulus): 
    def __init__(self,ang,m,M,R,g,t):
        super().__init__(ang,m,M,R,g)
        self.t = t #tiempo de oscilacion de la bala despues de alcanzar su angulo maximo
        self.ta = np.linspace(0,self.t,1000) #tiempo de oscilacion de la bala despues de alcanzar su angulo maximo

    def VelocidadMinima(self): #aplicando polimorfismo y calculando la velocidad minima para que el bloque de una vuelta
        return np.sqrt(4*self.g*self.R) #velocidad minima del sistema masa bloque para que de uan vuelta
    
    def VelocidadBala(self): #velocidad mnima de la bala para que de una vuelta (aplicamos polimorfismo)
        return self.VelocidadMinima()*(self.m + self.M)/self.m

    
    def movoscilatorio(self):  #movimiento oscilatorio del bloque despues de alcanzar su angulo maximo(suponemos ecuacion de MAS asi theta > 10)
                       #pero obvio esto es solo una aproximacion, pues tocaria solucionar nuemricamente la ecuacion diferencial
                        #y esto es tema del siguiente punto.
        theta = self.ang*np.sin(np.sqrt(self.g/self.R)*self.ta)
        x = self.R*np.sin(theta) #movimiento circular debido al MAS
        y = -self.R*np.cos(theta)
        return x,y

