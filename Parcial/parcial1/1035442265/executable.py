import math
from Tiro_Parabolico import TiroParabolico

if __name__=="__main__":

    #Llammar atributos

    vo = 10
    radtheta = math.radians(60)
    xo = 0
    ho = 10
    g = -9.8
    ax = 0#g/4
    
    t = TiroParabolico(vo,xo,ho,radtheta,g,ax)
    #print("velocidad inicial en x:{}".format(t.VoX()))
    #print("velocidad inicial en y:{}".format(t.VoY()))
    #print("tiempo de vuelo:{}".format(t.TiempoDeVuelo()))
    #print("array de tiempo:{}".format(t.aTime()))
    #print("velocidad en x:{}".format(t.VelocidaEnX()))
    #print("velocidad en y:{}".format(t.VelocidaEnY()))
    #print("El alcance maximo en x es:{}".format(t.AlcanceMax()))
    #print("la altura maxima en y es:{}".format(t.AlturaMax()))

    #print("posicion en x:{}".format(t.posX()))
    #print("posicion en y:{}".format(t.posY()))
    t.figMp()
    