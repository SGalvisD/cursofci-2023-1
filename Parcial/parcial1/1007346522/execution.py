from Movimientos2D import *
from PenduloBalistico import *
from Solucionador_EDO import *
from PenduloSimple import *

### --- Execution de todos los puntos ---


if __name__=='__main__':
    print('Name: {}'.format(__name__))


    ## ---- Punto 2. ----
    ## Código en: Movimientos2D.py 

    # # Agrege condiciones iniciales de su preferencia 
    x_0 = 0.
    y_0 = 0.
    v_0 = 10.
    angle = 45. # en grados
    a_y = -9.8 # Aceleración en y
    a_x = -100 # Aceleración en x
    
    # # -----Movimiento Parabólico-----
    #Particula = MovimientoParabolico(x_0,y_0,v_0,angle,g = a_y) # Inicializando
    #print(Particula.info()) # Imprime información de la trayectoria
    #Particula.Graf()        # Gŕafica de la trayectoria 
    #                         # Se puede observar en Plot_Movimiento.png

    # # -----Movimiento del Proyectil-----
    #Particula = MovimientoProyectil(x_0,y_0,v_0,angle,g = a_y,a_x = a_x) # Inicializando
    #print(Particula.info()) # Imprime información de la trayectoria
    #Particula.Graf()        # Gŕafica de la trayectoria
                            # Se puede observar en Plot_Movimiento.png

    # #-----Comparar ambos movimientos: -----
    #CompararMovimiento(x_0,y_0,v_0,angle,g = a_y,a_x = a_x) 
    # # Se puede observar en Plot_Movimiento_2D.png


    # # -----Péndulo Balístico ----
    ## Código en: PenduloBalistico.py 

    # # Condiciones Iniciales
    #MasaBala = 0.5
    #MasaBloque = 10
    #LongitudCuerda = 1
    #VelocidadBala = 200

    # # Inicializando
    #Pend_Balis = PenduloBalistico(MasaBala,MasaBloque,LongitudCuerda,v_bala = VelocidadBala)
    #print('La velocidad mínima del péndulo para que de una vuelta completa es',Pend_Balis.VelocidadMinima()) 
    #Pend_Balis.Grafica()  # Véase Plot_Oscilador_Balistico.png
    # # Como se va a notar, casi todos los objetos que se definen en este examen tienen
    # # el polimorfismo del método Graficar()

    ## ---- Punto 3. ----

    #--- Solución Numérica de Ecuaciones Diferenciales ---
    # Código en: Solucionador_EDO.py 

    # # Función a integrar
    #def FuncionEjemplo(x,y): return sympy.exp(-x) # Tal que y' = f(x,y)

    # # Condiciones iniciales
    # x0 = 0
    # y0 = -1

    # # Intervalo de la solución
    # a = 0
    # b = 1 

    # # Inicializando objeto
    # Ecu_dif = Solucion_EDO(FuncionEjemplo,x0,y0,a,b)

    # Ecu_dif.Grafica(method = 'RK4')       # Véase Plot_Solucion_EDO.png

    # Ecu_dif.GraficaComparacion(method= 'RK4') # Véase Plot_Soluciones_EDO.png


    # # --- Péndulo Simple ---

    # # Condiciones Iniciales
    # S_0 = [0,1]
    # Omega = 4 # Omega² = g/L
    # TiempoMax = 5

    # # Inicializando
    # pend = PenduloSimple(S_0,Omega,TiempoMax)
    # pend.GraficaComparacion(Coord= 0)  # Véase Plot_Pendulo_Simple.png 

