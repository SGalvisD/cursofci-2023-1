import numpy as np
from TiroParabolico import TiroParabolico
from TiroParabolico import PooTirop

'''La clase movimiento parabólico recive la posición y velocidad inicial
de una partícula, el ángulo de tiro y la acelaración en el eje x y y '''

if __name__ == "__main__":
    # valores iniciales
    x0 = 0
    y0 = 0
    v0 = 10000
    theta = 90
    ay = 9.8
    ax = 0.001#9.8 / 2

    # movimiento parabólico estándar, sin aceleración en x.
    ejemplo = TiroParabolico(x0, y0, v0, theta, ay, ax)
    print('la velocidad inicial en x es {} '.format(ejemplo.v0_x()), 'm/s')
    print('la velocidad inicial en y es {} '.format(ejemplo.v0_y()), 'm/s')
    print("el tiempo de vuelo es: {}".format(ejemplo.tiempo_vuelo()), "s")
    print("el alcance máximo en x es: {}".format(ejemplo.alcance_x()), "m")
    print("la altura máxima es: {}".format(ejemplo.altura()), "m")
    print(ejemplo.graficar())

    print('======================')
    d = 10
    ejemplo2 = PooTirop(x0, y0, v0, theta, d, ay)
    print('para alcanzar la distancia', d, 'm', 'con una velocidad inicial de ', v0, 'm/s', 'el ángulo de tiro es:', ejemplo2.angulo(), '°')