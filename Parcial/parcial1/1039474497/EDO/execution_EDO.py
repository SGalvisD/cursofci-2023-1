
from EDO import *


if __name__=="__main__":
    print("ejecutando metodo de Euler")

    #*****ingrese aqui los parametros para el metodo euler*******

    x0 = 0 #valor inicial variable indpeendiente
    y_x0 = -1 #valor inicial variable dependiente
    xmax = 1 #valor maximo variable independiente
    n = 100 #numero de puntos 

    
#************defina aqui su EDO como f**************
    def f(y, x):
        return np.exp(-x)
    
    #instancia de la clase EDOsolver para la EDO f = exp(-x) con CI y(0) = -1 en x E [0,1]

    solver = EDOSolver(f, y_x0, x0, xmax, n)
    solver.plot()
