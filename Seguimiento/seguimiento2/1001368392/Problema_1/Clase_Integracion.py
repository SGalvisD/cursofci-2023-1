import numpy as np
import matplotlib.pyplot as plt
import sympy as sy
class Integral():
    """
    La clase contiene metodos capaces de solucionar por metodo de Montecarlo y por metodo analitico
    la integral \int_{0}^{\pi} x^2 \cos x dx 
    
    No requiere parametros en su definicion ya que el numero de iteraciones de los metodos de montecarlo
    se toman como argumento en los metodos.
    """
    def __init__(self):
        self.x0 = 0
        self.xf = np.pi
        self.zero = np.pi/2
        self.arrxpos = np.arange(0, np.pi/2, 0.001)
        self.arrxneg = np.arange(np.pi/2, np.pi, 0.001)
        self.arrypos = self.arrxpos**2 * np.cos(self.arrxpos)
        self.arryneg = self.arrxneg**2 * np.cos(self.arrxneg)

    # Resuelve por metodo de montecarlo dadas N iteraciones:
    def solve_montecarlo(self,N):
        if N <= 1:
            raise Exception("El numero de iteraciones debe ser positivas")
        
        if N%1 != 0:
            raise Exception("El numero de iteraciones debe ser entero")

        def f(x):
            return x**2 * np.cos(x)
        
        # Parte de area positiva

        ymaxpos = np.max(self.arrypos)
        xrandpos = np.random.uniform(self.x0,self.zero,N)
        yrandpos = np.random.uniform(0,ymaxpos,N)

        condpos = (yrandpos <=  f(xrandpos))
        areapos = self.zero * ymaxpos * condpos.sum()/N
        
        # Parte de area negativa

        yminneg = np.min(self.arryneg)
        xrandneg = np.random.uniform(self.zero,self.xf,N)
        yrandneg = np.random.uniform(yminneg,0,N)

        condneg = (yrandneg >=  f(xrandneg))
        areaneg = self.zero * yminneg * condneg.sum()/N
        
        return areapos + areaneg
    
    # Resuelve de manera exacta usando sympy
    def solve_exact(self):
        x = sy.Symbol('x')
        sol = sy.integrate(x**2 * sy.cos(x), (x,self.x0, self.xf))
        return sol

    # Compara convergencia de la solucion por montecarlo con la solucion analitica
    def compare_sols(self, N):
        if N <= 1:
            raise Exception("El numero de iteraciones debe ser positivas")
        
        if N%1 != 0:
            raise Exception("El numero de iteraciones debe ser entero")
        
        Narr = range(1,N)
        solexact = self.solve_exact() * np.ones(N-1)

        solsMontecarlo = np.array([])
        for i in Narr:
            tempsol = self.solve_montecarlo(i)
            solsMontecarlo = np.append(solsMontecarlo, tempsol)

        plt.figure(figsize=(12,10))
        plt.plot(Narr, solsMontecarlo, label="Solucion por Montecarlo")
        plt.plot(Narr, solexact, label="Solucion exacta")
        plt.legend()
        plt.grid()
        plt.savefig("compsols.png")

