import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad

class Integral:
  def __init__(self,a,b,N,L):

    self.a=a
    self.b=b
    self.N=N
    self.L=L
   
  # Definimos la función a integrar
  def f(x):
      return (((2/self.L)**(1/2))*np.sin((2*np.pi*x)/self.L))**2
    
  def valores_integral(self):
    def f(x):
      return (((2/self.L)**(1/2))*np.sin((2*np.pi*x)/self.L))**2
    #lista vacia para llenar con los valores 
    valores =[]
    for i in range(self.N):
      self.N=10*(i+1)
  # Generamos N puntos aleatorios en el intervalo [a, b]
      x = np.random.uniform(self.a, self.b, self.N)
  # Evaluamos la función en los puntos generados
      
  # Calculamos la aproximación de la integral utilizando el método de Monte Carlo
      I_mc = (self.b-self.a) * np.mean(f(x))
      valores.append(I_mc)#introducir los valores en la lista vacia
    return valores

  def integral(self):
    return self.valores_integral()[-1]#valor integral montecarlo para 1000 iteraciones
  # Calculamos la integral exacta utilizando scipy.integrate.quad
  def integral_real(self):
    def f(x):
      return (((2/self.L)**(1/2))*np.sin((2*np.pi*x)/self.L))**2
    I_exact, _ = quad(f, self.a, self.b)
    return  I_exact





