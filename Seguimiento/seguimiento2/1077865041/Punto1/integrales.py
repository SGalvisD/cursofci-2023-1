#Resolver una integral con montecarlo
import numpy as np
import matplotlib.pyplot as plt
import random
from scipy.integrate import quad


#Definamos la funcion a integrar
def f(x):
    return np.cos(x)*(x**2)

#Clase para la integral
class integral:
    
    #Método constructor

    def __init__(self,a,b,N,N2  ):
        self.N = N #Numero de puntos
        self.a=a #Limite inferior
        self.b=b #Limite superior
        self.N2 = N2 #Numero de iteraciones para la grafica
        
    #Definamos la funcion que nos da el valor de la integral
    def integral_monte(self):
        suma=0
        for i in range(self.N):
            x=random.uniform(self.a,self.b)
            suma+=f(x)
        return (self.b-self.a)*(suma/self.N)

    #Resolvamos la integral de forma analitica con scipy
    def integral_analitica(self):
            I = quad(f, self.a, self.b)
            
            return np.round(I[0], 4)
    
    #Vamos a guardar los valores de la integral en cada iteracion y se grafican

    def grafica(self):
        lista1=[]
        lista2=[]
        for i in range(1,self.N2):
            self.N=i
            lista1.append(i)
            lista2.append(self.integral_monte())
        fig = plt.figure(figsize = (15,10))
        plt.plot(lista1,lista2, color='red', linestyle='-',label='Valor de la integral con Monte Carlo')
        plt.xlabel("Iteraciones",fontsize=15)
        plt.ylabel("Valor de la integral",fontsize=15)
        plt.title("Valor de la integral para distinta cantidad de iteraciones",fontsize=15)
        plt.grid()
        plt.axhline(self.integral_analitica(), color='black', linestyle='-',label='Valor analítico')
        plt.legend(fontsize=15)
        plt.savefig("integral.png")
        plt.show()
         


    