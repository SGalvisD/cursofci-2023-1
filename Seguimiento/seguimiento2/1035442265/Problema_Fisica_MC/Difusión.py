import numpy as np
import matplotlib.pyplot as plt


class difusión():

    def __init__(self,N1,N2,t):

        self.N1 = N1
        self.N2 = N2
        self.t = t
        #self.dt = dt
       

    def gamma(self):
        return np.random.uniform(0,1)
        
    def evolucion(self):
        for i in range(self.t):
            p = self.N1/(self.N1 + self.N2 )
            if self.gamma() < p:
                self.N1 = self.N1 - 1
                self.N2 = self.N2 + 1
            else:
                self.N1 = self.N1 + 1
                self.N2 = self.N2 - 1
            
        return self.N1,self.N2
