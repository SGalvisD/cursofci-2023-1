# Punto 1: Integración con Monte Carlo. Clase.

# Importo librerías.
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate

# Defino clase.
class solIntegral():
  # Método constructor.
  def __init__(self, a, b, N, f): # Se definen los atributos.
    self.a = a # Límite inferior de la integral.
    self.b = b # Límite superior de la integral.
    self.N = int(N) # Número de iteraciones, enteras.
    self.f = f # Función a integrar.
    self.ite = np.arange(1, N, 1) # Arreglo para iteraciones.

  def metExacto(self): # Método para solución analítica.
    sol = integrate.quad(self.f, self.a, self.b) # Función scipy.
    return np.round(sol[0], 5)
  
  def monteCarlo(self):
    s = 0
    n = int(10e3)
    for i in range(n):
      xrand = np.random.uniform(self.a, self.b)
      s += self.f(xrand)
      sol = (self.b - self.a)*(s/n)
    return sol

  def grafComp(self):
    fig = plt.figure(figsize=(12,8))
    solana = self.metExacto()
    solmonte = []
    for i in self.ite:
        solmonte.append(self.monteCarlo())

    plt.plot(self.ite, solmonte, color='blue', linestyle='-', label='Solución con Monte Carlo')
    plt.axhline(solana, color='black', linestyle='-',label='Solución analítica')
    plt.xlabel("Iteraciones")
    plt.ylabel("Valor de integral")
    plt.title("Valor numérico de integral por iteraciones con Monte Carlo")
    plt.legend()
    plt.grid()
    plt.savefig("grafCompP1.png")
    plt.show()