# Punto 2.  Barrera de Potencial Unidimensional

Supongamos un sistema Mecano-Cuántico con un Hamiltoniano no dependiente del tiempo, de manera que se puede escribir su ecuación de Autovalores como:

$$\hat{H} |\psi\rangle = E|\psi\rangle$$
en la representación posición
$$H\psi(x) = \left[ -\frac{\hbar²}{2m} \frac{d²}{dx²}+V(x) \right] \psi(x) = E\psi(x)\\
\Longleftrightarrow \left\{ \frac{d²}{dx²}-\frac{2m}{\hbar²} [V(x)-E] \right\} \psi(x) = 0$$

Ahora, veamos que $V(x)$ es un potencial de la forma
$$\displaystyle V(x) = \Big \{^{V_0\, ,\quad 0\; \leq\;x\;\leq L }_{0\, ,\quad x\; >\; 0\, \wedge \;x\;>\; L } $$
De manera que se tienen 3 funciones de onda $\psi_{I},\, \psi_{II}, \,\psi_{III}$ para las tres regiones que se definen por el potencial. Se imponen condiciones de fronteras apropiadas: continuidad entre regiones y que en $x\longrightarrow \infty$ la función sea acotada.

En nuestro caso nos interesa la función de onda cuando $E<V_0$, de manera que se llega que:

$$\psi_{I} = A_1\,e^{ikx} A'_1 e^{-ikx}\\
\psi_{II} = A_2\,e^{i\rho x}+ A'_2 e^{-i\rho x}\\
\psi_{III} = A_3\,e^{ikx}$$
Con $\displaystyle k = \sqrt{\frac{2mE}{\hbar²}}$ y $\displaystyle \rho = \sqrt{\frac{2m(E-V_0)}{\hbar²}}$

Las amplitudes de cada onda queda expresadas en términos de la Reflectancia $R$ y Transmitancia $T$, de manera que la suma de ambas sea 1. Es de particular importancia $T$ porque da cuenta de la probabilidad de que una partícula pase en relación a su Energía. Se calcula como:

$$T = \bigg|\frac{A_3}{A_1}\bigg|^2 = \bigg[1+\frac{V_0² \sinh²({\rho L })}{4E(V_0-E)}\bigg]^{-1}\\\, \\= \pi(E) = \frac{Particulas \; Transmitidas}{Total \; Particulas}$$

De manera que utilizando Métodos de MonteCarlo, se busca reconstruir esta probabilidad, al simular un número $N$ de partículas con Energía en el rango $(0,V_0)$ que se encuentran con la barrera de potencial y por propiedades cuánticas son capaces de Tunelar hacia después del potencial.