
''' Archivo de clase para resolución del problema de una partícula 
cargada en movimiento bajo los efectos de un campo magnético 
en z. No se presentará la solución analítica , sólo las ecuaciones en 
los momentos en que sea necesario definir los métodos. Se supone 
movimiento inicial asociado a una velocidad arbitraria.'''

# Importo librerías a usar.
import numpy as np
import matplotlib.pyplot as plt

# Defino clase a partir de función de python y respetando nomenclatura.
class trayPart():

    # Método constructor.
    def __init__(self, ek, theta, m, q, b, ite, n):

        # Atributos de clase.
        
        # Parámetros a usar, ingresados por el usuario.
        self. ek = ek/1.602e-19         # Energía cinética partícula [J].
        self.theta = np.radians(theta)  # Ángulo_incidencia(partícula,B) [rad].
        self.m = m                      # Masa partícula; electrón [kg].
        self.q = q                      # Carga partícula; electrón [C].
        self.b = b                      # Densidad flujo magnético [T].
        self.ite = ite                  # Iteraciones para arreglo tiempo.
        self.n = n                      # Repeticiones del periodo (espiras).
         
        # Parámetros adicionales necesarios.
        self.v0 = np.sqrt(2*self.ek/self.m)     # Velocidad inicial.
        self.w = (abs(self.q)*self.b)/(self.m)       # Frecuencia ciclotrón.

    # Métodos de la clase.
    
    # Periodo del movimiento helicoidal.
    def perT(self):
        per = (2*np.pi)/self.w
        return per
    
    # Método que determina arreglo de tiempo para simulación.
    def arrT(self):
        t = np.linspace(0,self.n*self.perT(),self.ite, 0.01) # Arreglo equidistante.
        return t
    
    # Velocidad perpendicular a B. Corresponde a aquella en el plano XY.
    # Si se quiere velocidad separada X y Y se debe tener el ángulo en el que 
    # incide la partícula en este plano.
    def vPerp(self):
        vxy = self.v0*round(np.sin(self.theta),3) # Round garantiza exactitud.
        return vxy
    
    # Velocidad paralela a B; i.e. velocidad en Z.
    def vPara(self):
        vz = self.v0*round(np.cos(self.theta),3)
        return vz
    
    # Radio del helicoide formado en el movimiento.
    def rHel(self):
        rhel = (self.m*self.v0)/(self.q*self.b)
        return rhel

    # Método posición X. Movimiento helicoidal; circulo en XY.
    def pX(self):
        x = self.rHel()*np.sin(self.w*self.arrT())
        return x

    # Método posición Y. Movimiento helicoidal; circulo en XY.
    def pY(self):
        y = self.rHel()*np.cos(self.w*self.arrT())
        return y
    
    # Método posición Z. Movimiento helicoidal; lineal en Z.
    def pZ(self):
        z = self.vPara()*self.arrT()
        return z

    # Método para gráfica 3D de trayectoria de la partícula.
    def figPt3D(self):
        plt.figure(figsize=(10,8))
        ax = plt.axes(projection="3d")
        ax.plot3D(self.pX(), self.pY(), self.pZ(), color = 'k' , marker = '.', label = 'Trayectoria Helicoidal.')
        ax.set_title("Trayectoria partícula cargada en campo uniforme", fontweight = "bold",fontsize = 16, )
        ax.set_xlabel("Posición X [m]")
        ax.set_ylabel("Posición Y [m]")
        ax.set_zlabel("Posición Z [m]")
        plt.legend()
        plt.tight_layout()
        plt.grid()
        plt.savefig('Tray3D_partícula_cargada_bUniforme.png')
        plt.show()
        return