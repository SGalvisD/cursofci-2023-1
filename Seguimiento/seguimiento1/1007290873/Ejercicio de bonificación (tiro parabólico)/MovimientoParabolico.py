import numpy as np
import math
import matplotlib.pyplot as plt
class movimientoParabolico(): 

    # Constructor

    def __init__(self, v_0, alpha, x_0, y_0, a, nombre_grafica):

        # Parámetros de la clase
        # v_0: vel. inicial [m/s]
        # alpha: ángulo [grados]
        # x_0: posición inicial en x [m]
        # y_0: posición inicial en y [m]
        # a : aceleración [m/s^2]
        # nombre_grafica: nombre que va a llevar la gráfica (string)

        # Definición de atributos

        self.vel_inicial = v_0
        self.angulo_grados = alpha
        self.angulo = math.radians(alpha) # ángulo en radianes
        self.pos_inicial_x = x_0
        self.pos_inicial_y = y_0
        self.acl_y = a
        self.vel_inicial_x = v_0 * np.cos(self.angulo)
        self.vel_inicial_y = v_0 * np.sin(self.angulo)
        self.nombre_grafica = nombre_grafica
        
    # Definición de métodos

    def x_posicion(self, t):
        return self.pos_inicial_x + self.vel_inicial_x * t

    def y_posicion(self, t):
        return self.pos_inicial_y + self.vel_inicial_y * t + 0.5 * self.acl_y * t**2

    def y_velocidad(self, t):
        return self.vel_inicial_y + self.acl_y * t

    def x_velocidad(self, t):
        return "Velocidad constante en x: {}".format(self.vel_inicial_x)

    def t_max(self): # Tiempo que corresponde a la altura máxima
        
        acl = float(self.acl_y)

        if acl == 0.0:
            raise ValueError("La aceleración debe ser distinta de 0")
        elif acl > 0:
            raise ValueError("No hay t máximo si la aceleración > 0")
        else:
            return -self.vel_inicial_y / acl
        

    def x_max(self): # Posición en x cuando se da la altura máxima
        return self.x_posicion(self.t_max())

    def y_max(self): # Altura máxima
        return self.y_posicion(self.t_max())

    def tiempo_arbitrario(self):
        # Tiempo arbitrario entre 5 y 30s, para los casos en que a > 0
        return np.random.randint(5, 30)
    
    def tiempo_vuelo(self):
        
        acl = float(self.acl_y)

        if acl == 0.0:
            raise ValueError("La aceleración debe ser distinta de 0") 
        elif acl > 0:
            # No hay un tiempo de vuelo, pero se regresa tiempo arbitrario para poder graficar
            return "Tiempo arbitrario: {}".format(self.tiempo_arbitrario())
        else:
            return ( - self.vel_inicial_y - np.sqrt( self.vel_inicial_y ** 2 - (2 * self.pos_inicial_y * acl) ) ) / acl
        

    def grafica_movimiento(self):

        # Array de tiempos:
        tiempos = np.linspace(0, self.tiempo_vuelo(), 100)
        # Arrays de las posiciones
        x_posiciones = self.x_posicion(tiempos)
        y_posiciones = self.y_posicion(tiempos)

        # Gráfica:
        fig, ax=plt.subplots(figsize=(18,15))
        ax.plot(x_posiciones, y_posiciones, color='red')
        ax.set_title("Gráfica del movimiento parabólico para las condiciones dadas", fontsize=20)
        ax.set_xlabel("$x(t)$ (m)",fontsize=20)
        ax.set_ylabel("$y(t)$ (m)",fontsize=20)
        ax.grid()
        fig.savefig(self.nombre_grafica)
        
class movimientoParabolicoConAire(movimientoParabolico):

    # Constructor

    def __init__(self, v_0, alpha, x_0, y_0, a, nombre_grafica, k):

        # Parámetros de la clase
        # v_0: vel. inicial [m/s]
        # alpha: ángulo [grados]
        # x_0: posición inicial en x [m]
        # y_0: posición inicial en y [m]
        # a: aceleración en y [m/s^2]
        # nombre_grafica: nombre que va a llevar la gráfica (string)
        # k: aceleración en x [m/s^2]

        # Definición de atributos

        super().__init__(v_0, alpha, x_0, y_0, a, nombre_grafica)
        self.acl_x = k 

    # Definición de métodos

    def x_posicion(self, t):
        return self.pos_inicial_x + self.vel_inicial_x * t + 0.5 * self.acl_x * t**2

    def x_velocidad(self, t):
        return self.vel_inicial_x + self.acl_x * t




    

