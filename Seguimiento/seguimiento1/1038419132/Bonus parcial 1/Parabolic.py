import numpy as np
import matplotlib.pyplot as plt

#esta clase clacula el movimiento en x y y de una parabola suponiendo que tiene resistencia de aire en el eje x,
#para parabolas sin friccion volver entonces entregar atributo a = 0

class parabo():

    def __init__(self,tiempo,gravedad,yini,veloini,angulo):
        self.t = tiempo #tiempo de vuelo de la partiucla
        self.g = gravedad #gravedad del problema
        self.yo = yini #oposicion inicial en y
        self.vo = veloini #velocidad inicial    
        self.grad = angulo ##angulo de disparo
        #self.a = ace #aceleracion en contra solo en eje x
    

    #método
    def motionx(self):
        x = self.vo * np.cos(np.pi/180*self.grad) * self.t  #defino metodo de mov en x
        return x

    def motiony(self):
        y = self.yo + self.vo * np.sin(np.pi/180*self.grad) * self.t -0.5*self.g*self.t**2 #def mov en y
        return y

#--------------------------------------------------------------------------------------------------   
class paraboa(parabo): #definiendo subclase 

    def __init__(self,tiempo,gravedad,yini,veloini,angulo,aceleracion): #llamo los atributos de la clase para ek contructor de la sub
        super().__init__(tiempo,gravedad,yini,veloini,angulo)
        self.a = aceleracion  #nuevo atributo solo para esta subclase

    def motionnx(self):  #redefino el método motion x introduciendo la aceleracion
        nix = self.vo * np.cos(np.pi/180*self.grad) * self.t  -0.5*self.a*self.t**2
        return nix




    
