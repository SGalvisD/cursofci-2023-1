
import numpy as np
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import axes3d


class trayectorias:
    # Inicialización.
    def __init__(self,B,ek,theta,m,q,t):
        self.B=B
        self.ek=ek
        self.theta=theta
        self.m=m
        self.q=q
        self.t=t
    # Veamos lasvelocidades
    def vx(self):
        v0= np.sqrt(2*self.ek/self.m)
        wc=(self.q*self.B)/self.m
        return v0*np.sin(self.theta)*np.cos(wc*self.t)

    def vy(self):
        v0= np.sqrt(2*self.ek/self.m)
        wc=(self.q*self.B)/self.m
        return -v0*np.sin(self.theta) * np.sin(wc*self.t)
    
    def vz(self):
        v0= np.sqrt(2*self.ek/self.m)
        return v0*np.cos(self.theta)
    
    #Veamos las posiciones
    def posx(self):
        v0= np.sqrt(2*self.ek/self.m)
        wc=(self.q*self.B)/self.m
        return v0*np.sin(self.theta)/wc * np.sin(wc*self.t)
    
    def posy(self):
        v0= np.sqrt(2*self.ek/self.m)
        wc=(self.q*self.B)/self.m
        return v0*np.sin(self.theta)/wc * (np.cos(wc*self.t)-1)
    
    def posz(self):
        return self.vz()*self.t
   
    #Grafiquemos

    def graph_p(self):

     
        fig = plt.figure()
        ax = plt.axes(projection='3d')
        ax.plot3D(self.posx(), self.posy(), self.posz(), 'red')

        ax.set_title('Trayectoria de la partícula cargada, en este caso un electrón.')
        ax.set_xlabel("X")
        ax.set_ylabel("Y")
        ax.set_zlabel("Z")
        plt.savefig("Trayectoria.png")
        plt.legend()
    
        plt.show()
